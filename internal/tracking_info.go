package internal

import (
	"database/sql"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/auth"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/blockchainrepo"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/blockchainrepo/blockchain"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/client"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/config"
	"log"
	"time"
)

func StartTracking(loadConfig config.Config, db *sql.DB) {
	notifyClient, err := client.NewNotifierClient(loadConfig.NotifierAddr)
	if err != nil {
		log.Println(err)
		return
	}

	blockchains := []blockchain.NodesBlockChain{
		&blockchain.Cardano{Blockchain: "cardano"},
	}

	nodeRep := blockchainrepo.InitNodeRepository(notifyClient, blockchains, auth.Auth, loadConfig, db)
	nodeRep.ConnectNodeRepositoryServices(loadConfig)
	for {
		nodeRep.ProcessStatistic()
		time.Sleep(time.Second * time.Duration(loadConfig.TimeoutCheck))
		nodeRep.ConnectNodeRepositoryServices(loadConfig)
	}
}

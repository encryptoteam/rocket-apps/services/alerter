package nodesinfo

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/Knetic/govaluate"
	"github.com/tidwall/gjson"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/database/model"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/msgsender"
	"gitlab.com/encryptoteam/rocket-apps/services/alerter/internal/nodesinfo/checker"
	pb "gitlab.com/encryptoteam/rocket-apps/services/proto/proto-gen/notifier"
	"log"
	"strconv"
	"time"
)

const msgTemplateType = "Node %s, field %s, cheker type: %s"

// CheckFieldsOfNode - checking fields of node and create notifiers messages
func CheckFieldsOfNode(newNode interface{}, oldNode interface{},
	alerts []model.AlertNodeAndAlert) (map[msgsender.KeyMsg]msgsender.BodyMsg, error) {

	newNodeJSON, err := json.Marshal(&newNode)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	messages := map[msgsender.KeyMsg]msgsender.BodyMsg{}
	for _, alert := range alerts {
		value := gjson.Get(string(newNodeJSON), alert.CheckedField)
		if !value.Exists() {
			log.Println("val not exist")
			continue
		}

		var oldValue string
		if oldNode != nil {
			oldNodeJSON, _ := json.Marshal(&oldNode)
			oldVal := gjson.Get(string(oldNodeJSON), alert.CheckedField)
			if !value.Exists() {
				log.Println("val not exist")
				continue
			}
			oldValue = oldVal.String()
		}

		//  func checkField(alert, oldValue, value) (isValid bool, msg string, err error)
		diffVal, err := calculateDiffVal(alert, oldValue, value.String())
		if err != nil {
			log.Println(err)
			continue
		}

		msg, valid, err := createMsgIfDiffNotValid(alert, diffVal, value.String())
		if err != nil {
			log.Println(err)
			continue
		} else if valid {
			continue
		}

		messagesKey := msgsender.KeyMsg{
			NodeUuid:  alert.NodeUuid,
			NodeField: alert.CheckedField,
		}

		messages[messagesKey] = msg
	}

	return messages, nil
}

// example : formula = diffVal ** 0.5
func calculateFrequency(diffValue float64, formula string) (float64, error) {
	expression, err := govaluate.NewEvaluableExpression(formula)
	if err != nil {
		log.Println(err)
		return 0, err
	}

	parameters := make(map[string]interface{}, 8)
	parameters["diffVal"] = diffValue

	result, err := expression.Evaluate(parameters)
	if err != nil {
		log.Println(err)
		return 0, err
	}

	frequency, isFloat := result.(float64)
	if !isFloat {
		return 0, errors.New("cant calculate frequency")
	}

	return frequency, nil
}

func calculateDiffVal(alert model.AlertNodeAndAlert, oldValue string, value string) (float64, error) {
	var diffVal float64
	var err error

	switch alert.TypeChecker {
	case checker.IntervalT.String():
		diffVal, err = checker.IntervalCalculate(alert.NormalFrom,
			alert.NormalTo, value)
		if err != nil {
			log.Println(err)
			return 0, err
		}
	case checker.ChangeUpT.String():
		if oldValue == "" {
			return 0, errors.New("old val does not exist")
		}
		diffVal, err = checker.ChangeUpCalculate(oldValue, value)
		if err != nil {
			log.Println(err)
			return 0, err
		}
	case checker.ChangeDownT.String():
	case checker.DateT.String():
		tm, err := time.Parse("2006-01-02 15:04:05 -0700 MST", value)
		if err != nil {
			log.Println(err)
			return 0, err
		}
		diffVal, err = checker.DateDiffCalculate(tm)
		if err != nil {
			log.Println(err)
			return 0, err
		}
	case checker.EqualT.String():
		if oldValue == "" {
			return 0, errors.New("old val does not exist")
		}
		diffVal, err = checker.EqualCheck(value, oldValue)
		if err != nil {
			log.Println(err)
			return 0, err
		}
	case checker.MoreT.String():
		diffVal, err = strconv.ParseFloat(value, 64)
		if err != nil {
			log.Println(err)
			return 0, err
		}
	case checker.CheckCardanoVer.String():
		outdated, err := checker.IsCardanoVersionOutdated(value)
		if err != nil {
			log.Println(err)
			return 0, err
		}
		if outdated {
			return 1, nil
		} else {
			return 0, nil
		}
	case checker.CheckBool.String():
		diffVal, err = checker.GetIntBool(value)
		if err != nil {
			log.Println(err)
			return 0, err
		}
	default:
		log.Println("undefined checker type")
		return 0, errors.New("undefined checker type")
	}

	return diffVal, nil
}

func createMsgIfDiffNotValid(alert model.AlertNodeAndAlert,
	diffVal float64, value string) (msg msgsender.BodyMsg, valid bool, err error) {
	msg = msgsender.BodyMsg{
		Notify: &pb.SendNotifier{},
	}

	if diffVal > alert.CriticalTo || diffVal < alert.CriticalFrom {
		msg.Frequency = msgsender.MaxFrequency
		msg.Notify.From = fmt.Sprintf("%f", alert.CriticalFrom)
		msg.Notify.To = fmt.Sprintf("%f", alert.CriticalTo)
	} else if diffVal > alert.NormalTo || diffVal < alert.NormalFrom {
		frequency, err := calculateFrequency(diffVal, alert.Frequency)
		if err != nil {
			log.Println(err)
			return msg, true, err
		}
		msg.Frequency = int64(frequency)
		msg.Notify.From = fmt.Sprintf("%f", alert.NormalFrom)
		msg.Notify.To = fmt.Sprintf("%f", alert.NormalTo)
	} else {
		return msgsender.BodyMsg{}, true, nil
	}

	msg.Notify.CurrentVal = value
	msg.Notify.TextMessage = fmt.Sprintf(msgTemplateType, alert.NodeUuid, alert.CheckedField, alert.TypeChecker)

	return msg, false, nil
}
